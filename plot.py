#!/usr/bin/python
import argparse

parser = argparse.ArgumentParser(description='Visulize densvr\'s data as bar chart.')
parser.add_argument('--min',help='minimum cnt to be displayed',default=0,type=int)
parser.add_argument('--top',help='display top n results (if this value is zero, all will be displaed, if this value is below zero, bottom n will be displayed)',default=0,type=int)
parser.add_argument('--log',help='use logarithmic scale for y axis',default=False,type=bool)
parser.add_argument('--outfile',help='save figure to file provided',default='',type=str)
parser.add_argument('--infile',help='load data from file provided',default='vkbigdata.txt',type=str)
parser.add_argument('--outdata',help='print data to file provided',default='',type=str)
args = parser.parse_args()
print('args: '+str(args))
import json
data=[]
with open(args.infile) as data_file:        
    for line in data_file:
    	line=line.strip()
    	if len(line)>0:
    		data.append(json.loads(line))
res=dict()
for d in data:	
	o=res.get(d.get('id'))
	if o==None:
		res[d.get('id')]=d			
	else:
		res[d.get('id')]['cnt']+=d.get('cnt')
if len(args.outdata)>0:
	with open(args.outdata, 'w') as outfile:
		for k in res.keys():						
			outfile.write(json.dumps(res[k])+'\n')

import numpy as np
import matplotlib.pyplot as plt
import pylab as pl
import numpy as np
d = dict()
for key in sorted(res):
	value=res[key]
	if(value>=args.min):
		d[key]=res[key].get('cnt')
if(args.top!=0):
	top=args.top
	if(top>0):		
		d = dict(sorted(d.iteritems(), reverse=False)[:top])
	else:
		d = dict(sorted(d.iteritems(), reverse=True)[:-top])		
print(d)
X = np.arange(len(d))
f = pl.figure()
pl.bar(X, d.values(), width=0.5,log=args.log)
pl.xticks(X, d.keys())
locs, labels = pl.xticks()
pl.setp(labels, rotation=90)
ymax = max(d.values()) + 1
pl.ylim(0, ymax)
if len(args.outfile)>0:	
	sizey=ymax/25
	if args.log:
		sizey/=25
	f.set_size_inches(len(d)/5,sizey)
	pl.savefig(args.outfile,bbox_inches='tight')
else:	
	pl.show()
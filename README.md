# plot #
```
/plot.py -h
usage: plot.py [-h] [--min MIN] [--top TOP] [--log LOG] [--outfile OUTFILE]
               [--infile INFILE] [--outdata OUTDATA]

Visulize densvr's data as bar chart.

optional arguments:
  -h, --help         show this help message and exit
  --min MIN          minimum cnt to be displayed
  --top TOP          display top n results (if this value is zero, all will be
                     displaed, if this value is below zero, bottom n will be
                     displayed)
  --log LOG          use logarithmic scale for y axis
  --outfile OUTFILE  save figure to file provided
  --infile INFILE    load data from file provided
  --outdata OUTDATA  print data to file provided
```
# Output #
![vkbigdata.png](https://bitbucket.org/repo/74zLe5/images/2621496778-vkbigdata.png)
![vkbigdata.svg](https://bytebucket.org/vkbigdata/vkplot/raw/master/plots/vkbigdata.svg)
![vkbigdata.log.svg](https://bytebucket.org/vkbigdata/vkplot/raw/master/plots/vkbigdata.log.svg)